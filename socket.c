/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

 #include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef struct addrinfo address;
typedef address (*addr_initializer)(address* addr);

address init_addr_generic (address* addr);
addr_initializer init_addr = &init_addr_generic;


address init_addr_tcp(address* addr) {
    memset(addr, 0, sizeof *addr); //zero it out
    addr->ai_family = AF_UNSPEC; //Use whatever (IPv4 || IPv6)
    addr->ai_socktype = SOCK_STREAM; //use SOCK_STREAM (TCP)
    addr->ai_flags = AI_PASSIVE;
}

address init_addr_udp(address* addr) {
    memset(addr, 0, sizeof *addr); //zero it out
    addr->ai_family = AF_UNSPEC; //Use whatever (IPv4 || IPv6)
    addr->ai_socktype = SOCK_DGRAM; //SOCK_DGRAM (UDP)
    addr->ai_flags = AI_PASSIVE;
}


address init_addr_generic (address* addr) {
    if (n3config.isTCP)
        init_addr = &init_addr_tcp;
    else
        init_addr = &init_addr_udp;

    return init_addr(addr);
}
