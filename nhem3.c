/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

#include <stdbool.h>        //bool type
#include <stdlib.h>         //exit(int), strtol(char*), and others
#include <string.h>         //strcmp and other char utilities
#include <pthread.h>        //threads
#include <fcntl.h>          //open();
#include <unistd.h>         //close();
#include <sys/wait.h>       //Wait
#include <signal.h>         //Signal Action
#include <errno.h>          //errno

#include "logger.c"         //Verbosity Filter

#include "runtimeConfig.c"  //Runtime Configuration container struct and parsing function

#include "socket.c"         //Internal socket functions and data

#include "server.c"         //Server functions and data
#include "client.c"         //Client ^

void on_kill(int sig) {
    if (sig == SIGINT)
        Log("\nHave a nice day!\n");
    close(sock_fd);
    close(new_fd);
    exit(EXIT_SUCCESS);
}

int main (int argc, char *argv[]) {
    Log("Hi, this is nhem3\n\n");

    signal(SIGINT, on_kill);

    //Initialize global configuration struct (see runtimeConfig.c)
    init_config(argc, argv);

    if (n3config.isServer)
        n3serve();
    else
        n3connect();
}
