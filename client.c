/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

#define MAX_DATA_LENGTH 128

address client_address;

int numbytes = 0;
char buf[MAX_DATA_LENGTH];

void n3connect () {
    init_hints();

    try_bind_or_connect(&connect);

    read_fd = sock_fd;

    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        exit(2);
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
            s, sizeof s);

    pthread_create(&recv_t, NULL, recv_thread, NULL);

    if (!n3config.isTCP) {
        write(sock_fd, "Hello Server, with love, Client.\n", sizeof(message));
    }

    do {
        send_message(sock_fd);
        sleep(0.1);
    } while (true);
    //close(sock_fd);
}
