/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

#include <ctype.h>

#define DEF_HOST_IP_URL "127.0.0.1\0"
#define DEF_SERVER_IP "0.0.0.0\0"
#define DEF_PORT "33333\0"
#define DEF_IS_TCP false
#define DEF_IS_SERVER false

typedef struct RUNTIME_NHEM3_CONFIG {
    char* host; //"127.0.0.1"
    char* port; //"33333\0"
    bool isTCP; //false
    bool isServer; //false
} runtime_config;

runtime_config n3config;

void PrintHelp();

void TryGetPort (int argc, char *argv[], int* i) {
    if (argc - 1 > *i && isdigit(argv[*i+1][0])){
        *i = *i + 1;
        n3config.port = argv[*i];
        Log ("Port selected: %s\n", n3config.port);
    } else {
        Log ("No port found, will use default %s\n", DEF_PORT);
    }
}


void check_option (int argc, char *argv[], int* i) {
    static bool initServerHost, initTCP = false;
    if (strcmp(argv[*i], "-S") == 0 && !initServerHost) {
        initServerHost = true;

        Log ("Server Mode.\n");
        n3config.isServer = true;
        n3config.host = DEF_SERVER_IP;

        TryGetPort(argc, argv, i);

    } else if (argv[*i][0] != '-' && !n3config.isServer && !initServerHost) {
        initServerHost = true;

        Log("Client Mode, host addr: %s\n", argv[*i]);
        n3config.host = argv[*i];
        TryGetPort(argc, argv, i);

        //n3config.isServer = false;

    } else if (strcmp(argv[*i], "-t") == 0 && !initTCP){
        initTCP = true;
        n3config.isTCP = true;
        Log("TCP Selected.\n");
    } else if (strcmp(argv[*i], "-q") == 0 || strcmp(argv[*i], "--quiet") == 0){
        SetQuietMode();
    } else if (strcmp(argv[*i], "-h") == 0 || strcmp(argv[*i], "--help") == 0){
        PrintHelp();
    }

}


void init_config (int argc, char *argv[]) {
    if (argc == 1) {
        PrintHelp(argv[0]);
    }

    //Initialize configuration with default values.
    n3config.host = DEF_HOST_IP_URL;
    n3config.port = DEF_PORT;
    n3config.isTCP = DEF_IS_TCP;
    n3config.isServer = DEF_IS_SERVER;

    for (int i = 1; i < argc; ++i)
        check_option (argc, argv, &i);

    setbuf(stdout, 0);
}


void PrintHelp (char * name) {
    Log("How to use this:");
    Log("\n%s [-h] \t\t#Show this message", name);
    Log("\n%s -S \t\t#Run on server mode", name);
    Log("\n%s 127.0.0.1 \t#Run on client mode with host 127.0.0.1", name);
    Log("\n%s [-S|host] 54321 \t#Run on port 54321", name);
    Log("\n%s [-S|host] -t \t#Run on TCP mode", name);
    Log("\n%s [-S|host] -q \t#Run on quiet mode", name);
    Log("\n");

    exit(0);
}
