/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

#include <stdio.h>
#include <stdarg.h>

typedef int (*logger)(const char * format, ...);

logger Log = *printf;


//Does Nothing
int debugLogQuiet (const char * format, ...) {
    return 0;
}


void SetQuietMode () {
    Log = *debugLogQuiet;
}
