/**
 * Code licensed under GNU General Public License V3.
 * http://www.gnu.org/licenses/
 */

#define MAX_MSG_LENGTH 512

typedef int (*connection_func)(int sockfd, const struct sockaddr *addr, socklen_t addrlen);

address server_address;

int read_fd;
int sock_fd, new_fd;  // listen on sock_fd, new connection on new_fd

address hints, *res, *p;
struct sockaddr_storage their_addr; // connector's address information

socklen_t sin_size;
struct sigaction sig_action;

char s[INET6_ADDRSTRLEN];
int rv;

char message[MAX_MSG_LENGTH];

pthread_t recv_t;

void child_signal_handler(int s) {
    // waitpid() might overwrite errno, so we save and restore it:
    int saved_errno = errno;

    while(waitpid(-1, NULL, WNOHANG) > 0);

    errno = saved_errno;
}


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


void init_hints() {
    init_addr(&hints);

    if ((rv = getaddrinfo(n3config.host, n3config.port, &hints, &res)) != 0) {
        Log("getaddrinfo -- %s\n", gai_strerror(rv));
        exit(1);
    }
}


void try_bind_or_connect(connection_func try_connect) {

    init_hints();

    for(p = res; p != NULL; p = p->ai_next) {
        if ((sock_fd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("socket");
            continue;
        }

        int yes = 1;
        if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) == -1) {
            perror("setsockopt");
            exit(1);
        }

        if (new_fd = try_connect(sock_fd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sock_fd);
            perror("Can't stablish connection.");
            continue;
        }

        break;
    }

    freeaddrinfo(res); // all done with this structure

    if (p == NULL)  {
        fprintf(stderr, "Looks like I can't bind, bye!\n");
        exit(1);
    }
}


int receive_message() {
    int size_recv = 0;

    memset(message, 0, MAX_MSG_LENGTH);
    if(read(read_fd, message, sizeof(message)) > 0) {
        printf(">> %s", message);

    }
}


void receive_message_udp() {
    socklen_t src_addr_len = sizeof(p->ai_addr);
    if (recvfrom(sock_fd, message, sizeof(message), 0, p->ai_addr, &src_addr_len) > 0 ){
        printf(">> %s", message);
        connect(read_fd, p->ai_addr, p->ai_addrlen);
    }
}


void *recv_thread() {
    if (n3config.isTCP || !n3config.isServer){
        do {
            receive_message();
            sleep(0.5);
        } while (true);
    } else {
        do {
            receive_message_udp();
            sleep(0.5);
        } while (true);
    }
}


void send_message(int s) {
    fflush(stdout);
    if( fgets (message, MAX_MSG_LENGTH, stdin) != NULL ) {
        while (message[0] == '\n')
            memcpy(message, &message[1], strlen(message));
        write(s, message, sizeof(message));
    }
}


void send_message_udp(int s) {
    socklen_t src_addr_len  = sizeof(their_addr);
    fflush(stdout);
    if( fgets (message, MAX_MSG_LENGTH, stdin) != NULL ) {
        while (message[0] == '\n')
            memcpy(message, &message[1], strlen(message));
        if (n3config.isServer)
            sendto(sock_fd, message, sizeof(message), 0, (struct sockaddr*) &their_addr, src_addr_len);
        else
            send(s, message, sizeof(message), 0);
    }
}


void wait_for_handshake() {
    socklen_t src_addr_len  = sizeof(their_addr);
    recvfrom(sock_fd, message, sizeof(message), 0, (struct sockaddr *)&their_addr, &src_addr_len);
    Log("\nHandshake message received: \n\t%s\n", message);
}


void n3serve() {

    try_bind_or_connect(&bind);

    if (n3config.isTCP && (listen(sock_fd, 10) == -1)) {
        perror("listen");
        exit(1);
    }

    Log("Hey, I'm waiting for connections!\n");

    sin_size = sizeof their_addr;
    if (n3config.isTCP) {
        new_fd = accept(sock_fd, (struct sockaddr *)&their_addr, &sin_size);
        read_fd = new_fd;
    } else {
        wait_for_handshake();
    }

    if (new_fd == -1) {
        perror("accept");
        exit(0);
    }

    inet_ntop(their_addr.ss_family,
        get_in_addr((struct sockaddr *)&their_addr),
        s, sizeof s);

    pthread_create(&recv_t, NULL, recv_thread, NULL);
    if (n3config.isTCP) {
        do {
            send_message(new_fd);
            sleep(0.1);
        } while(true);
    } else {
        do {
            send_message_udp(new_fd);
            sleep(0.1);
        } while(true);
    }
}
